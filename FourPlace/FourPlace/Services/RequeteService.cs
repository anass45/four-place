﻿using Common.Api.Dtos;
using MonkeyCache.SQLite;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TD.Api.Dtos;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace FourPlace
{
    public class RequeteService : RequeteService.IRequestService
    {
        public interface IRequestService
        {
            Task<T> Get<T>(string url) where T : class;
            Task<T> Post<T>(string url, object dataToSend) where T : class;
            Task<T> Patch<T>(string url, object dataToSend) where T : class;
            Task<Response<ImageItem>> uploadImage(string url, byte[] imageData);
            Boolean TestConnection();
            void SetTokenData(LoginResult l);
            LoginResult GetTokenData();
            void RefreshToken();
        }
        //BASE URL
        public static readonly String baseURl = "https://td-api.julienmialon.com";
        //USER URL
        public static readonly String loginURL = baseURl + "/auth/login";
        public static readonly String refreshURL = baseURl + "/auth/refresh";
        public static readonly String registerURL = baseURl + "/auth/register";
        public static readonly String userURL = baseURl + "/me";
        public static readonly String userPasswordURL = userURL + "/password";
        //PLACE URL
        public static readonly String imagesURL = baseURl + "/images";
        public static readonly String placesURL = baseURl + "/places";
        //token
        private static LoginResult tokenData;
        //monkey cache
        public static readonly String cacheListeLieux = "cacheListeLieux";
        public static readonly String cacheTokenData = "cacheTokenData";
        public static readonly int dureeCache = 6;//6 jours

        void IRequestService.SetTokenData(LoginResult token)
        {
            tokenData = token;
            Barrel.Current.Add(cacheTokenData, tokenData, TimeSpan.FromMilliseconds(token.ExpiresIn));

        }

        LoginResult IRequestService.GetTokenData()
        {
            return tokenData;
        }

        public static async Task TestApi()
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await client.GetAsync(baseURl);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(responseBody);
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
                }
            }
        }

        async Task<T> IRequestService.Post<T>(string url, object dataToSend)
        {
            bool checkToken = CheckToken();
            if (!TestConnection() && !checkToken){ return null;}
            using (HttpClient client = new HttpClient())
            {
                try
                {   //On set le token access dans le header si il existe
                    if (checkToken){client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenData.TokenType, tokenData.AccessToken);}
                    var JsonDataToSend = JsonConvert.SerializeObject(dataToSend);
                    HttpContent content = new StringContent(JsonDataToSend, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PostAsync(url, content);

                    var responseBody = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(responseBody);
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine("Message :{0} ", e.Message);
                }
                catch (Exception e )
                {
                    Console.WriteLine("\nException - Erreur"+e.Message);
                }
            }
            return null;
        }

        async Task<T> IRequestService.Get<T>(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                if (CheckToken())
                {//On set le token access dans le header si il existe
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenData.TokenType, tokenData.AccessToken);
                }
                var json = string.Empty;
                if (!TestConnection())//si pas de connection chargement du barrel si donnée existante
                {
                    if (!Barrel.Current.IsExpired(url))
                    {
                        json = Barrel.Current.Get<string>(url);
                    }
                }
                try
                {
                    if (string.IsNullOrWhiteSpace(json))
                    {
                        json = await client.GetStringAsync(url);
                        Barrel.Current.Add(url, json, TimeSpan.FromDays(dureeCache));
                    }
                    return JsonConvert.DeserializeObject<T>(json);
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine("Message :{0} ", e.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Unable to get information from server {ex}");
                }
                return default(T);
            }
        }     

        async Task<T> IRequestService.Patch<T>(string url, object dataToSend)
        {
            if (!TestConnection())
            {
                return null;
            }
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    if (CheckToken())
                    {
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenData.TokenType, tokenData.AccessToken);
                    }
                    var request = new HttpRequestMessage(new HttpMethod("PATCH"), url)
                    {
                        Content = new StringContent(
                        JsonConvert.SerializeObject(dataToSend), Encoding.UTF8, "application/json")
                    };
                    var response = await client.SendAsync(request);
                    var responseBody = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(responseBody);
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine("\nException - Erreur" + e.Message);
                }
                return null;
            }
        }

        async Task<Response<ImageItem>> IRequestService.uploadImage(string url, byte[] imageData)
        {
            if (!TestConnection())
            {
                return null;
            }
            HttpClient client = new HttpClient();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url);

            request.Headers.Authorization = new AuthenticationHeaderValue(tokenData.TokenType, tokenData.AccessToken);
            MultipartFormDataContent requestContent = new MultipartFormDataContent();

            var imageContent = new ByteArrayContent(imageData);
            imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");
            
            requestContent.Add(imageContent, "file", "file.jpg");
            request.Content = requestContent;

            HttpResponseMessage response = await client.SendAsync(request);
            string responseBody = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Response<ImageItem>>(responseBody);
        }

        public Boolean TestConnection()
        {
            if (Connectivity.NetworkAccess == NetworkAccess.Internet)
            {// Connection to internet is available
                return true;
            }
            return false;
        }

        async void IRequestService.RefreshToken()
        {
            if (TestConnection())
            {
                RefreshRequest refreshRequest = new RefreshRequest();
                refreshRequest.RefreshToken = tokenData.RefreshToken;
                var result = await DependencyService.Get<RequeteService.IRequestService>().Post<Response<LoginResult>>(refreshURL, refreshRequest);
                if (result != null && result.IsSuccess)
                {
                    DependencyService.Get<RequeteService.IRequestService>().SetTokenData(result.Data);
                }
                else{ tokenData = null;}
            }
        }

        public bool CheckToken()
        {
            if (tokenData != null) return true;
            /*else
            {
                var StockageToken = Barrel.Current.Get<LoginResult>(cacheTokenData);
                if (StockageToken != null)
                {
                    tokenData = StockageToken;
                    return true;
                }
                else //si la periode de validation du token est depassé on lance le refresh token
                {
                    DependencyService.Get<RequeteService.IRequestService>().RefreshToken();
                    return false;
                }
            }*/
            return false;  
        }
    }
}