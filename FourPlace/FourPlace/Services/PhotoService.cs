﻿using FourPlace.Services;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Storm.Mvvm.Forms;
using System.IO;
using Xamarin.Forms;

namespace FourPlace
{
    class PhotoService
    {
        private BaseContentPage page;
        private Image image;
        private byte[] fileByte;
        public byte[] GetFileByte() { return fileByte; }
        public Image GetImage() { return image; }

        public PhotoService(BaseContentPage page,Image image) {
            this.page = page;
            this.image = image;
        }

        public async void ChoisirPhotoAction()
        {   
            if (!CrossMedia.Current.IsPickVideoSupported)
            {
                 DependencyService.Get<AffichageService.IAffichageService>().AfficherMessage(page, "Erreur", "Choix de photo impossible", "OK");
                return;
            }
            var file = await CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
            {
                PhotoSize = PhotoSize.Small,
            });
            if (file == null)
                return;
            else
            {
                var memoryStream = new MemoryStream();
                file.GetStream().CopyTo(memoryStream);
                fileByte = memoryStream.ToArray();
                image.Source = ImageSource.FromStream(() =>
                {
                    using (file)
                    {
                        return file.GetStream();
                    }
                });
            }
        }

        public async void UseCameraAction()
        {
            if (!CrossMedia.Current.IsTakePhotoSupported)
            {
                DependencyService.Get<AffichageService.IAffichageService>().AfficherMessage(page, "Erreur", "Prise de photo impossible", "OK");
                return;
            }
            var photo = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                PhotoSize = PhotoSize.Small,
            });
            if (photo == null)
                return;
            else
            {
                DependencyService.Get<AffichageService.IAffichageService>().AfficherMessage(page, "File Location", photo.Path, "OK");
                image.Source = ImageSource.FromStream(() => { return photo.GetStream(); });
                var memoryStream = new MemoryStream();
                photo.GetStream().CopyTo(memoryStream);
                memoryStream.Dispose();
                fileByte = memoryStream.ToArray();
            }
        }
    }
}
