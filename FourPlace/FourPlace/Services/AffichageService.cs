﻿using Storm.Mvvm.Forms;
using System;
using System.Collections.Generic;
using System.Text;

namespace FourPlace.Services
{
    class AffichageService : AffichageService.IAffichageService
    {
        public interface IAffichageService
        {
            void AfficherMessage(BaseContentPage page, string titre, string message, string messageBouton);
            void ErreurEnregistrement(BaseContentPage page);
            void Erreur(BaseContentPage page);
            void ErreurChampsVides(BaseContentPage page);
        }

        async void IAffichageService.AfficherMessage(BaseContentPage page, string titre, string message, string messageBouton)
        {
            await page.DisplayAlert(titre, message, messageBouton);
        }

        async void IAffichageService.ErreurEnregistrement(BaseContentPage page)
        {
            await page.DisplayAlert("Erreur", "Impossible d'enregistrer les modifications", "OK");
        }

        async void IAffichageService.Erreur(BaseContentPage page)
        {
            await page.DisplayAlert("Erreur", "Une erreur est survenue", "OK");
        }

        async void IAffichageService.ErreurChampsVides(BaseContentPage page)
        {
            await page.DisplayAlert("Erreur", "Champs vides", "OK");
        }
    }
}
