﻿using Storm.Mvvm.Forms;
using Xamarin.Forms.Xaml;

namespace FourPlace.Inscription
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Inscription : BaseContentPage
    {
		public Inscription ()
		{
			InitializeComponent ();
            BindingContext = new InscriptionViewModel(this);
		}
	}
}