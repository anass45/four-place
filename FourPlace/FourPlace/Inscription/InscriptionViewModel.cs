﻿using System;
using Storm.Mvvm;
using System.Windows.Input;
using Xamarin.Forms;
using TD.Api.Dtos;
using Common.Api.Dtos;
using FourPlace.Services;

namespace FourPlace.Inscription
{
    class InscriptionViewModel : ViewModelBase
    {
        private String _email;
        private String _firstname;
        private String _lastname;
        private String _password;
        private Inscription page;
        private bool isEnabledButton;
        public bool IsEnabledButton { get => isEnabledButton; set => SetProperty(ref isEnabledButton, value); }
        public ICommand InscriptionCommand { get; }  
        public String Email {get => _email; set => SetProperty(ref _email, value);} 
        public String Firstname{get => _firstname; set => SetProperty(ref _firstname, value);}
        public String Lastname{get => _lastname;set => SetProperty(ref _lastname, value);}
        public String Password{get => _password;set => SetProperty(ref _password, value);}

        public InscriptionViewModel(Inscription inscription)
        {
            this.page = inscription;
            InscriptionCommand = new Command(InscriptionAction);
            IsEnabledButton = true;
        }

        private async void InscriptionAction(object sender)
        {
            IsEnabledButton = false;
            if (!CheckData())
            {
                DependencyService.Get<AffichageService.IAffichageService>().ErreurChampsVides(page);
                IsEnabledButton = true;
                return;
            }
            RegisterRequest user = new RegisterRequest();
            user.Email = _email;
            user.FirstName = _firstname;
            user.LastName = _lastname;
            user.Password = _password;
            var result = await DependencyService.Get<RequeteService.IRequestService>().Post<Response<LoginResult>>(RequeteService.registerURL, user);
            if (result != null && result.IsSuccess)
            {
                DependencyService.Get<RequeteService.IRequestService>().SetTokenData(result.Data);
                await page.Navigation.PopToRootAsync();
            }
            else
            {
                DependencyService.Get<AffichageService.IAffichageService>().AfficherMessage(page, "Erreur", "Impossible de s'inscrire", "OK");
                IsEnabledButton = true;
            }
        }

        public bool CheckData()
        {
            if (Email == null || Firstname == null || Lastname == null || Password == null)
            {
                return false;
            }
            return true;
        }
    }
}
