﻿using Common.Api.Dtos;
using FourPlace.Services;
using Storm.Mvvm;
using System;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlace.Connexion
{
    class ConnexionViewModel: ViewModelBase
    {
        private String _email;
        private String _password;
        private Connexion page;
        private bool isEnabledButton;
        public bool IsEnabledButton { get => isEnabledButton; set => SetProperty(ref isEnabledButton, value); }
        public ICommand ConnexionCommand { get; }  
        public String Email{get => _email;set => SetProperty(ref _email, value);}    
        public String Password{get => _password;set => SetProperty(ref _password, value);}

        public ConnexionViewModel(Connexion connexion)
        {
            this.page = connexion;
            this.ConnexionCommand = new Command(ConnexionAction);
            Email = "€";
            Password = "$";
            IsEnabledButton = true;
        }

        private async void ConnexionAction(object sender)
        {
            IsEnabledButton = false;
            if (!CheckData())
            {
                DependencyService.Get<AffichageService.IAffichageService>().ErreurChampsVides(page);
                IsEnabledButton = true;
                return;
            }
            LoginRequest user = new LoginRequest();
            user.Email = _email;
            user.Password = _password;
            var result = await DependencyService.Get<RequeteService.IRequestService>().Post<Response<LoginResult>>(RequeteService.loginURL, user);
            if (result != null && result.IsSuccess)
            {
                DependencyService.Get<RequeteService.IRequestService>().SetTokenData(result.Data);
                await page.Navigation.PopToRootAsync();
            }
            else
            {
                DependencyService.Get<AffichageService.IAffichageService>().AfficherMessage(page, "Erreur", "Impossible de se connecter", "OK");
                IsEnabledButton = true;
            }
        }

        public bool CheckData()
        {
            if (Email == null ||  Password == null)
            {
                return false;
            }
            return true;
        }
    }
}
