﻿using Storm.Mvvm.Forms;
namespace FourPlace.Connexion
{
    public partial class Connexion : BaseContentPage
    {
		public Connexion ()
		{
			InitializeComponent ();
            BindingContext = new ConnexionViewModel(this);
		}
	}
}