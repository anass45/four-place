﻿using Storm.Mvvm.Forms;
using TD.Api.Dtos;

namespace FourPlace.Lieu
{
    public partial class Lieu : BaseContentPage
    {
        public Lieu (PlaceItemSummary item)
		{
			InitializeComponent ();
            BindingContext = new LieuViewModel(this,item);     
		}
	}
}