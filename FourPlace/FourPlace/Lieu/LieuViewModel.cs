﻿using Common.Api.Dtos;
using FourPlace.Services;
using Storm.Mvvm;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
namespace FourPlace.Lieu
{
    class LieuViewModel : ViewModelBase
    {
        private PlaceItemSummary item;
        private Lieu page;
        private String _commentaire;
        private ObservableCollection<CommentItem> _commentItem;
        private string _comment;
        private bool isEnabledButton;
        public bool IsEnabledButton { get => isEnabledButton; set => SetProperty(ref isEnabledButton, value); }
        public ObservableCollection<CommentItem> CommentItem { get => _commentItem; set => SetProperty(ref _commentItem, value); }
        public ICommand CommenterCommand { get; }
        public String Commentaire{get => _commentaire;set => SetProperty(ref _commentaire, value);}
        public string Comment{get => _comment;set => SetProperty(ref _comment, value);}
        public string Url{get => item.Url;}
        public string Title{get => item.Title; }
        public string Description{get => item.Description;}

        public LieuViewModel(Lieu page, PlaceItemSummary item)
        {
            this.page = page;
            this.item = item;
            this.CommentItem = new ObservableCollection<CommentItem>();
            CommenterCommand = new Command(CommenterAction);
            IsEnabledButton = true;
        }

        public override async Task OnResume()
        {
            await base.OnResume();
            IsEnabledButton = true;
            GetDetailLieu();
        }
        private async void GetDetailLieu()//afficher commentaire seulement si internet
        {
            var result = await DependencyService.Get<RequeteService.IRequestService>().Get<Response<PlaceItem>>(RequeteService.placesURL+"/"+item.Id);
            if (result != null && result.IsSuccess)
            {
                CommentItem.Clear();
                foreach (CommentItem comment in result.Data.Comments)
                {
                    CommentItem.Add(comment);
                }
                SetMapLocation();
            }
        }

        private async void CommenterAction(object sender)
        {
            IsEnabledButton = false;
            if (!CheckData())
            {
                DependencyService.Get<AffichageService.IAffichageService>().ErreurChampsVides(page);
                IsEnabledButton = true;
                return;
            }
            CreateCommentRequest commentaire = new CreateCommentRequest();
            commentaire.Text = Commentaire;
            var result = await DependencyService.Get<RequeteService.IRequestService>().Post<Response>(RequeteService.placesURL+"/"+item.Id+"/comments", commentaire);
            if (result != null && result.IsSuccess)
            {
                GetDetailLieu();
                Commentaire = "";
            }
            else
            {
                DependencyService.Get<AffichageService.IAffichageService>().Erreur(page);
            }
            IsEnabledButton = true;
        }

        private void SetMapLocation()
        {
            Map map = ((Map)page.FindByName("map"));
            map.MoveToRegion(MapSpan.FromCenterAndRadius(
                        new Position(item.Latitude, item.Longitude),
                        Distance.FromKilometers(15)
                        ));
        }

        public bool CheckData()
        {
            if (Commentaire == null)
            {
                return false;
            }
            return true;
        }
    }
}
