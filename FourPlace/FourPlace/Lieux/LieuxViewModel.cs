﻿using Common.Api.Dtos;
using FourPlace.Services;
using Storm.Mvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlace.Lieux
{
    class LieuxViewModel : ViewModelBase
    {
        private Lieux page;
        private ObservableCollection<PlaceItemSummary> _placeItem;
        public ObservableCollection<PlaceItemSummary> PlaceItem{get => _placeItem;set => SetProperty(ref _placeItem, value);}

        public LieuxViewModel(Lieux page)
        {
            this.page = page;
            this.PlaceItem= new ObservableCollection<PlaceItemSummary>();
        }

        public override async Task OnResume()
        {
            await base.OnResume();
            GetLieux();
        }

        private async void GetLieux(){
            var result = await DependencyService.Get<RequeteService.IRequestService>().Get<Response<List<PlaceItemSummary>>>(RequeteService.placesURL);
            if (result != null && result.IsSuccess)
            {
                PlaceItem.Clear();
                foreach (PlaceItemSummary place in result.Data)
                {
                    PlaceItem.Add(place);
                }
            }
            else
            {
                DependencyService.Get<AffichageService.IAffichageService>().Erreur(page);
                await page.Navigation.PopToRootAsync();
            }
        }     
    }
}
