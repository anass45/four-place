﻿using Storm.Mvvm.Forms;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlace.Lieux
{
    public partial class Lieux : BaseContentPage
    {
        public Lieux ()
		{
			InitializeComponent ();
            BindingContext = new LieuxViewModel(this);
        }

        async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            PlaceItemSummary place = ((PlaceItemSummary)e.Item);
            Lieu.Lieu lieu = new Lieu.Lieu(place);
            await this.Navigation.PushAsync(lieu);
        }
    }
}