﻿using Storm.Mvvm.Forms;

namespace FourPlace.Profil
{
    public partial class Profil : BaseContentPage
    {
		public Profil ()
		{
			InitializeComponent ();
            BindingContext = new ProfilViewModel(this);
        }
	}
}