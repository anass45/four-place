﻿using Common.Api.Dtos;
using FourPlace.Services;
using Storm.Mvvm;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlace.Profil
{
    class ProfilViewModel : ViewModelBase
    {
        private Profil page;
        private Boolean _ismodifierProfil;
        private Boolean _ismodifierPassword;
        private String _email;
        private String _firstname;
        private String _lastname;
        private String _password;
        private String _oldPassword;
        private PhotoService servicePhoto;
        private Image image;
        private byte[] fileByte;
        private bool isEnabledButton;
        public bool IsEnabledButton { get => isEnabledButton; set => SetProperty(ref isEnabledButton, value); }
        public ICommand UseCameraCommand { get; }
        public ICommand ChoisirPhotoCommand { get; }
        public ICommand ModifierProfilCommand { get; }
        public ICommand ModifierPasswordCommand { get; }
        public ICommand SaveCommand { get; }
        public Boolean IsModifierProfil{ get => _ismodifierProfil; set => SetProperty(ref _ismodifierProfil, value);}
        public Boolean IsModifierPassword { get => _ismodifierPassword; set => SetProperty(ref _ismodifierPassword, value); }
        public String Email{ get => _email; set => SetProperty(ref _email, value);}
        public String Firstname{ get => _firstname; set => SetProperty(ref _firstname, value);}
        public String Lastname{ get => _lastname; set => SetProperty(ref _lastname, value);}
        public String Password{ get => _password; set => SetProperty(ref _password, value);}
        public String OldPassword{get => _oldPassword;set => SetProperty(ref _oldPassword, value); }

        public ProfilViewModel(Profil page)
        {
            this.page = page;
            image = ((Image)page.FindByName("Image"));
            ModifierProfilCommand = new Command(ModifierProfilAction);
            ModifierPasswordCommand = new Command(ModifierPasswordAction);         
            UseCameraCommand = new Command(UseCameraActionAsync);
            ChoisirPhotoCommand = new Command(ChoisirPhotoActionAsync);
            SaveCommand = new Command(SaveAction);
            IsEnabledButton = true;
            getDataProfil();
        }

        public override async Task OnResume()
        {
            await base.OnResume();
            IsEnabledButton = true;
        }
        private async void getDataProfil()
        {
            var result = await DependencyService.Get<RequeteService.IRequestService>().Get<Response<UserItem>>(RequeteService.userURL);
            if (result != null && result.IsSuccess)
            {
                this.Firstname = result.Data.FirstName;
                this.Lastname = result.Data.LastName;
                this.Email = result.Data.Email;
                this.image.Source = RequeteService.imagesURL + "/" + result.Data.ImageId;
            }
        }

        private void ModifierProfilAction(object sender)
        {
            IsModifierProfil = true;
            Button btnPassword = page.Content.FindByName<Button>("modifierPassword");
            Button btnProfil = page.Content.FindByName<Button>("modifierProfil");
            Button btnSave = page.Content.FindByName<Button>("save");
            if (btnPassword != null && btnProfil != null) {
                btnPassword.IsVisible = false;
                btnProfil.IsVisible = false;
                btnSave.IsVisible = true;
            }
        }

        private void ModifierPasswordAction(object sender)
        {
            IsModifierPassword = true;
            Button btnPassword = page.Content.FindByName<Button>("modifierPassword");
            Button btnProfil = page.Content.FindByName<Button>("modifierProfil");
            Button btnSave = page.Content.FindByName<Button>("save");
            if (btnPassword != null && btnProfil != null)
            {
                btnPassword.IsVisible = false;
                btnProfil.IsVisible = false;
                btnSave.IsVisible = true;
            }
        }

        private async void SaveAction(object sender)
        {
            IsEnabledButton = false;
            if (IsModifierProfil)
            {
                int idPhoto = 0;
                if (servicePhoto != null)
                {
                    image = servicePhoto.GetImage();
                    fileByte = servicePhoto.GetFileByte();
                    if (fileByte != null)
                    {
                        var resultPhoto = await DependencyService.Get<RequeteService.IRequestService>().uploadImage(RequeteService.imagesURL, fileByte);
                        if (resultPhoto != null && resultPhoto.IsSuccess)
                        { idPhoto = resultPhoto.Data.Id; }
                        else
                        {
                            DependencyService.Get<AffichageService.IAffichageService>().Erreur(page);
                            IsEnabledButton = true;
                        }
                    }
                }       
                UpdateProfileRequest update = new UpdateProfileRequest();
                update.FirstName = _firstname;
                update.LastName = _lastname;
                if (idPhoto != 0) { update.ImageId = idPhoto; }
                var result = await DependencyService.Get<RequeteService.IRequestService>().Patch<Response<UserItem>>(RequeteService.userURL, update);
                if (result != null && result.IsSuccess)
                {
                    DependencyService.Get<AffichageService.IAffichageService>().AfficherMessage(page, "Succes", "Profil modifié", "OK");
                    await page.Navigation.PopToRootAsync();
                }
                else
                {
                    DependencyService.Get<AffichageService.IAffichageService>().Erreur(page);
                }
            }
            else if (IsModifierPassword)
            {
                if(_oldPassword==null|| _password == null)
                {
                    DependencyService.Get<AffichageService.IAffichageService>().ErreurChampsVides(page);
                }
                else
                {
                    UpdatePasswordRequest update = new UpdatePasswordRequest();
                    update.OldPassword = _oldPassword;
                    update.NewPassword = _password;
                    var result = await DependencyService.Get<RequeteService.IRequestService>().Patch<Response>(RequeteService.userPasswordURL, update);
                    Console.WriteLine(result);
                    if (result != null && result.IsSuccess)
                    {
                        DependencyService.Get<AffichageService.IAffichageService>().AfficherMessage(page, "Succes", "Password modifié", "OK");
                        await page.Navigation.PopToRootAsync();
                    }
                    else
                    {
                        DependencyService.Get<AffichageService.IAffichageService>().Erreur(page);
                    }
                }
            }
            IsEnabledButton = true;
        }

        private void ChoisirPhotoActionAsync()
        {
            servicePhoto = new PhotoService(page, image);
            servicePhoto.ChoisirPhotoAction();
        }

        private void UseCameraActionAsync()
        {
            servicePhoto = new PhotoService(page, image);
            servicePhoto.UseCameraAction();
        }
    }
}
