﻿using FourPlace.Services;
using MonkeyCache.SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace FourPlace
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new MainPage.MainPage());
            //services
            DependencyService.Register<RequeteService.IRequestService, RequeteService>();
            DependencyService.Register<AffichageService.IAffichageService, AffichageService>();
            //monkey cache 
            Barrel.ApplicationId = "FourPlace";
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
