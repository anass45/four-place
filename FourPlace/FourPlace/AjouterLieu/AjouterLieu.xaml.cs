﻿using Storm.Mvvm.Forms;

namespace FourPlace.AjouterLieu
{
    public partial class AjouterLieu : BaseContentPage
    {
		public AjouterLieu ()
		{
			InitializeComponent ();
            BindingContext = new AjouterLieuViewModel(this);
        }
	}
}