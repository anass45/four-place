﻿using Storm.Mvvm;
using System.Windows.Input;
using Xamarin.Forms;
using System;
using TD.Api.Dtos;
using Common.Api.Dtos;
using Xamarin.Essentials;
using FourPlace.Services;

namespace FourPlace.AjouterLieu
{
    class AjouterLieuViewModel : ViewModelBase
    {
        private AjouterLieu page;
        private PhotoService servicePhoto;
        private Image image;
        private String _title;
        private String _description;
        private Double _latitude;
        private Double _longitude;
        private byte[] fileByte;
        private bool isEnabledButton;
        public bool IsEnabledButton { get => isEnabledButton; set => SetProperty(ref isEnabledButton, value); }
        public ICommand UseCameraCommand { get; }
        public ICommand ChoisirPhotoCommand { get; }
        public ICommand AjouterLieuCommand { get; }  
        public String Title { get => _title; set => SetProperty(ref _title, value);}
        public String Description { get => _description; set => SetProperty(ref _description, value); }
        public Double Latitude { get => _latitude; set => SetProperty(ref _latitude, value); }
        public Double Longitude { get => _longitude; set => SetProperty(ref _longitude, value); }

        public AjouterLieuViewModel(AjouterLieu ajouterLieu)
        {
            page = ajouterLieu;
            image = ((Image)page.FindByName("Image"));
            UseCameraCommand = new Command(UseCameraActionAsync);
            ChoisirPhotoCommand = new Command(ChoisirPhotoActionAsync);
            AjouterLieuCommand = new Command(AjouterLieuAction);
            IsEnabledButton = true;
            GetLocationAsync();
        }

        private async void AjouterLieuAction()
        {
            IsEnabledButton = false;
            if (!CheckData())
            {
                DependencyService.Get<AffichageService.IAffichageService>().ErreurChampsVides(page);
                IsEnabledButton = true;
                return;
            }
            if (servicePhoto != null)
            {
               //recupere information sur l'image (tableau byte) la methode async pour obtenir la photo est fini
                image = servicePhoto.GetImage();
                fileByte = servicePhoto.GetFileByte();
                //upload image
                if (fileByte != null)
                {
                    var result = await DependencyService.Get<RequeteService.IRequestService>().uploadImage(RequeteService.imagesURL, fileByte);
                    if (result != null && result.IsSuccess)
                    {
                        CreatePlaceRequest request = new CreatePlaceRequest();
                        request.Title = Title;
                        request.Description = Description;
                        request.ImageId = result.Data.Id;
                        request.Latitude = Latitude;
                        request.Longitude = Longitude;
                        //post lieu
                        var res = await DependencyService.Get<RequeteService.IRequestService>().Post<Response>(RequeteService.placesURL, request);
                        if (res != null && res.IsSuccess)
                        {
                            DependencyService.Get<AffichageService.IAffichageService>().AfficherMessage(page, "Succes", "Lieu ajouter", "OK");
                            await page.Navigation.PopToRootAsync();
                            return;
                        }
                    }
                }
            }
            else
            {
                DependencyService.Get<AffichageService.IAffichageService>().AfficherMessage(page, "Error", "Veuillez ajouter une image", "OK");
                IsEnabledButton = true;
                return;
            }
            DependencyService.Get<AffichageService.IAffichageService>().Erreur(page);
           IsEnabledButton = true;
        }

        public bool CheckData()
        {
            if (Title == null || Description == null || Latitude == 0 || Longitude == 0)
            {
                return false;
            }
            return true;
        }

        private void  ChoisirPhotoActionAsync()
        {
           servicePhoto = new PhotoService(page,image);
           servicePhoto.ChoisirPhotoAction();
        }

        private void UseCameraActionAsync()
        {
             servicePhoto = new PhotoService(page,image);
             servicePhoto.UseCameraAction();
        }

        private async void GetLocationAsync()
        {
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Medium);
                var location = await Geolocation.GetLocationAsync(request);

                if (location != null)
                {
                    this.Latitude = location.Latitude;
                    this.Longitude = location.Longitude;
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                Console.WriteLine("\nException - Erreur" + fnsEx.Message);
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
                Console.WriteLine("\nException - Erreur" + fneEx.Message);
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                Console.WriteLine("\nException - Erreur" + pEx.Message);
            }
            catch (Exception ex)
            {
                // Unable to get location
                Console.WriteLine("\nException - Erreur" + ex.Message);
            }
        }
    }
}
