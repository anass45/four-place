﻿using Storm.Mvvm;
using System;
using System.Windows.Input;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace FourPlace.MainPage
{
    class MainViewModel: ViewModelBase
    {
        private MainPage mainPage;
        private Boolean _isConnected;
        private bool isEnabledButton;
        public bool IsEnabledButton { get => isEnabledButton; set => SetProperty(ref isEnabledButton, value); }
        public ICommand OnAjouterLieuCommand { get; }
        public ICommand OnLieuxCommand { get; }
        public ICommand OnInscriptionCommand{get;}
        public ICommand OnLoginCommand{get;}
        public ICommand OnProfilCommand { get; }
        public Boolean IsConnected{get => _isConnected;set => SetProperty(ref _isConnected, value); }

        public MainViewModel(MainPage mainPage)
        {
            this.mainPage = mainPage;
            this.OnInscriptionCommand = new Command(OnInscriptionAction);
            this.OnLoginCommand = new Command(OnLoginAction);
            this.OnProfilCommand = new Command(OnProfilAction);
            this. OnAjouterLieuCommand= new Command(OnAjouterLieuAction);
            this.OnLieuxCommand =new Command(OnLieuxAction);
            IsConnected = false;
            IsEnabledButton = true;
        }

        public override async Task OnResume()
        {
            await base.OnResume();
            IsEnabledButton = true;
            if (DependencyService.Get<RequeteService.IRequestService>().GetTokenData()!= null)
            {
                IsConnected = true;
            }
            else
            {
                IsConnected = false;
            }
        }

        private async void OnLoginAction(object sender)
        {
            IsEnabledButton = false;
            await mainPage.Navigation.PushAsync(new Connexion.Connexion());
        }

        private async void OnProfilAction(object sender)
        {
            IsEnabledButton = false;
            await mainPage.Navigation.PushAsync(new Profil.Profil());
        }

        private async void OnInscriptionAction(object sender)
        {
            IsEnabledButton = false;
            await mainPage.Navigation.PushAsync(new Inscription.Inscription());
        }

        private async void OnAjouterLieuAction(object sender)
        {
            IsEnabledButton = false;
            await mainPage.Navigation.PushAsync(new AjouterLieu.AjouterLieu());
        }

        private async void OnLieuxAction(object sender)
        {
            IsEnabledButton = false;
            await mainPage.Navigation.PushAsync(new Lieux.Lieux());
        }
    }
}
