﻿using Storm.Mvvm.Forms;

namespace FourPlace.MainPage
{
    public partial class MainPage : BaseContentPage
    {
        public MainPage()
        {          
            InitializeComponent();
            BindingContext = new MainViewModel(this);  
        }  
    }
}
